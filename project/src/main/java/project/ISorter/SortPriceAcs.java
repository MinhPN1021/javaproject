
package project.ISorter;
import java.util.*;

import project.Product;
/**
 * Class that sorts by Price Ascending by using Comparator and compare.
 */
public class SortPriceAcs  implements Comparator<Product> {
    @Override
    public int compare( Product item1, Product item2){
        return Double.compare(item1.getPrice(), item2.getPrice());
    }
    
}
