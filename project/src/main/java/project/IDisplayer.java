package project;


/**
 * Interface IDisplayer which is a placeholder.
 */
public interface IDisplayer {
    void displayProducts();
    int displayActions();
}
