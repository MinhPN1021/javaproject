package project;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import org.junit.Test;

public class fileManipulationTest {
    /* Tests for the Admin class, testing for loadClothingItems  */
    @Test
    public void testLoadClothingItems() throws IOException {
        // Specify the path to your data file
        File file = new File("src/main/java/project/data/testing1.txt");
        String absolutePath = file.getAbsolutePath();
        System.out.println("Absolute path is: " + absolutePath);
        Path testFilePath = Paths.get("src/main/java/project/data/testing1.txt");
        FileProductImporter importer = new FileProductImporter(testFilePath);
        
        
        // Call the method being tested
        List<ClothingItems> clothingItems = importer.loadClothingItems();
        

        for(ClothingItems item: clothingItems) {
            System.out.println(item.getName() + " Price: " + item.getPrice());
        }
        System.out.println(clothingItems.size());
        // Assert specific properties of the loaded clothing items
        System.out.println(importer.getCounter());
        assertEquals(importer.getCounter(), clothingItems.size()); // Assuming there should be 2 items
    }

}
