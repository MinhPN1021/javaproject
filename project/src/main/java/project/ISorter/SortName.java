package project.ISorter;
import java.util.*;

import project.ClothingItems;
/**
 * Class that sorts by the Name of the product 
 */
public class SortName implements Comparator<ClothingItems> {
    @Override
    public int compare(ClothingItems clothingItem1, ClothingItems clothingItem2) {
        return clothingItem1.getName().compareTo(clothingItem2.getName());
    }
}
