package project.ISorter;
import java.util.Comparator;

import project.ClothingItems;
/**
 * Class that sorts by Material using Comparator and compare.
 */
public class SortMaterial implements Comparator<ClothingItems>{
    @Override
    public int compare (ClothingItems item1, ClothingItems item2) {
        return item1.getMaterial().compareTo(item2.getMaterial());
    }
}
