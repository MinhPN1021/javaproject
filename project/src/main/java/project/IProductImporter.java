package project;
import java.util.List;
/**
 * Interface IProductImporter which is a placeholder to load Clothing Items.
 */
public interface IProductImporter {
    List<ClothingItems> loadClothingItems();
}
