package project;
import java.util.*;


public class ClothingHotItems extends ClothingItems {

    private String heatConductivity;
    private String waterAbsorbtion;
    private String uvProtection;
    
    /**
     * Constructor for ClothingHotItems
     * @param name
     * @param price
     * @param category
     * @param size
     * @param material
     * @param quantity
     * @param HeatCond
     * @param WaterAbsorbtion
     * @param UVProtection
     */
    public ClothingHotItems(String name, double price, String category,String size,String material,int quantity,String HeatCond, String WaterAbsorbtion,String UVProtection){
        super(name, price, category, size, material, quantity);
        this.heatConductivity= HeatCond;
        this.waterAbsorbtion = WaterAbsorbtion;
        this.uvProtection = UVProtection;
    }

    /**
     * Getters method
     * @return this.field
     */
    public String getHeatConductivity(){
        return this.heatConductivity;
    }

    public String getWaterAbsorbtion(){
        return this.waterAbsorbtion;
    }

    public String getUVProtection(){
        return this.uvProtection;
    }
    @Override
    public String toString(){
        String s = ("Name: " + this.getName() + " | Price: " + this.getPrice() + " | Size: "+ this.getSize()+" | Material: " + this.getMaterial() + ", "+ this.getHeatConductivity() + " | Water Absortion: "+ this.getWaterAbsorbtion() + " | UV Protection: "+ this.getUVProtection());
        return s;
    }
   

    /**
     * Creates a new ClothingHotItems object by taking user input.
     *
     * @return The newly created ClothingHotItems object.
     */
    public static ClothingHotItems addItem(){
        Scanner reader =new Scanner(System.in);
        String name = Helper.getValidStringInput("Enter product name (no special characters): ", "^[a-zA-Z0-9 ]*$");
        double price = Helper.getValidDoubleInputPrice("Enter product price: $");
        String size = Helper.getValidStringInput("Enter product size (M, L, S): ", "^[MLS]$");
        String material = Helper.getValidStringInput("Enter product material", "^[a-zA-Z]*$");
        int qtyInStock = Helper.getValidIntInput("Enter product quantity in stock: ");
        String heatConductivity = Helper.getValidStringInput("Enter product heat conductivity (Breathable/Restricted): ", "^(Breathable|Restricted)$");
        String waterAbsorption = Helper.getValidStringInput("Enter product water absorption (Low/Medium/High/None): ", "^(Low|Medium|High|None)$");
        String uvProtected = Helper.getValidStringInput("Enter product UV protection (Low/Medium/High): ", "^(Low|Medium|High)$");
        ClothingHotItems item = new ClothingHotItems(name,price,"HOT",size,material,qtyInStock,heatConductivity,waterAbsorption,uvProtected);
        
        return item;
    }

    /**
     * For the sake of separating the business layer and data, this will get the item and transform it into Strings.
     */
    public String ExportToString(){
        return this.getCategory()+","+this.getName()+","+this.getPrice()+","+this.getSize()+","+this.getMaterial()+","+this.getQuantity()+","+this.getHeatConductivity()+","+this.getWaterAbsorbtion()+","+this.getUVProtection();
    }
}
