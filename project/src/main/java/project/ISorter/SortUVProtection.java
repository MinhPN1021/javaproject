package project.ISorter;
import java.util.Comparator;

import project.ClothingColdItems;
import project.ClothingHotItems;
import project.ClothingItems;

/**
 * Class that sorts by UVProtection if its Low/Medium/High, its alphabetical by using Comparator and compare.
 */
public class SortUVProtection implements Comparator<ClothingItems> {

    @Override
    public int compare(ClothingItems item1, ClothingItems item2) {
        
        if (item1 instanceof ClothingHotItems && item2 instanceof ClothingHotItems) {
            ClothingHotItems hotItem1 = (ClothingHotItems) item1;
            ClothingHotItems hotItem2 = (ClothingHotItems) item2;

            // Compare by UVProtection (High to Low)
            return hotItem1.getUVProtection().compareTo(hotItem2.getUVProtection());
        } else if (item1 instanceof ClothingColdItems && item2 instanceof ClothingColdItems) {
            // If both items are Cold items, maintain the original order
            return 0;
        } else if (item1 instanceof ClothingHotItems) {
            // Hot items come first
            return -1;
        } else if (item2 instanceof ClothingHotItems) {
            // Hot items come first
            return 1;
        }
        return 0;
    }
}
