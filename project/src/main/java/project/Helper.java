package project;

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

public class Helper {
    
     private static final Scanner scanner = new Scanner(System.in);

     public static int getValidInput(int min, int max, String prompt) {
        int input;
        do {
            System.out.println(prompt);
            while (!scanner.hasNextInt()) {
                System.out.println("Invalid input. Please enter a number.");
                scanner.nextLine(); // consume the invalid input
            }
            input = scanner.nextInt();
            scanner.nextLine();
        } 
        while (input < min || input > max);

        return input;
    }


    // Helper method to validate string input
    public static String getValidStringInput(String prompt, String regex) {
        String userInput="";
        do {
            System.out.println(prompt);
            userInput = scanner.nextLine();
        } 
        while (!userInput.matches(regex));
        return userInput;
    }

    // Helper method to validate double input
    public static double getValidDoubleInput(String prompt, String regex) {
        double userInput;
        do {
            System.out.println(prompt);
            while (!scanner.hasNextDouble() ) {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.next(); // consume the invalid input
            }
            userInput = scanner.nextDouble();
            scanner.nextLine(); // consume the newline character
        } 
        while (userInput <= 0 || userInput > 1);
        return userInput;
    }
    
    public static double getValidDoubleInputPrice(String prompt) {
        double userInput;
        do {
            System.out.println(prompt);
            while (!scanner.hasNextDouble()) {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.next(); // consume the invalid input
            }
            userInput = scanner.nextDouble();
            scanner.nextLine(); // consume the newline character
        } 
        while (userInput <= 0);
        return userInput;
    }

    // Helper method to validate integer input
    public static int getValidIntInput(String prompt) {
        int userInput;
        do {
            System.out.println(prompt);
            while (!scanner.hasNextInt()) {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.next(); // consume the invalid input
            }
            userInput = scanner.nextInt();
        } 
        while (userInput < 0);
        scanner.nextLine(); // consume the newline character
        return userInput;
    }
    /**
     * Writes to the file path by resetting it to 0 and then writes it as per the first line's order Category,Name etc
     * @param path
     */
    public static void CleanFile(String path){
        try{
            Path p = Paths.get(path);
            
            List<String> line =Arrays.asList("Category - Name - Price - Size - Material - Quantity in stock - (Water Proofness-Insulation-Layers-Accessory) or (Heat Conductivity-Water Absorbtion-UV Protection)");

            Files.write(p, line, StandardCharsets.UTF_8, StandardOpenOption.CREATE,StandardOpenOption.TRUNCATE_EXISTING);
        
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }


    /**
     * Helper method to write to the file using a path and an item that has been transformed to a string
     * @param path
     * @param item
     */

    public static void writeToFile(String path, String item){
        try{
            List<String> lines = Arrays.asList(item);
            Path p = Paths.get(path);
            Files.write(p, lines, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
            System.out.println("Added new item successfully");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

}
