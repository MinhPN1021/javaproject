package project.ISorter;

import java.util.Comparator;

import project.ClothingItems;
/**
 * Class that sorts by category using Comparator and compare.
 */
public class SortByCategory implements Comparator<ClothingItems>{

    @Override
    public int compare(ClothingItems item1, ClothingItems item2) {
        return item1.getCategory().compareTo(item2.getCategory());
    }
    
}
