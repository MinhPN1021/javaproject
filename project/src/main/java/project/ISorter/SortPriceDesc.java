
package project.ISorter;
import java.util.*;

import project.Product;
/**
 * Class that sorts by Price descending by using Comparator and compare.
 */
public class SortPriceDesc  implements Comparator<Product> {
    @Override
    public int compare( Product item1, Product item2){
        return -Double.compare(item1.getPrice(), item2.getPrice());
    }
    
}
