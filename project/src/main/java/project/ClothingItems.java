package project;

/**
 * A class ClothingItems which extends from Product. Has its own fields + the fields of Product.
 */
public class ClothingItems extends Product {
    private String category;
    private String size;
    private String material;
    private int quantity;
    
    /**
     * Constructor for Clothing Items
     * @param name
     * @param price
     * @param category
     * @param size
     * @param material
     * @param quantity
     */
    public ClothingItems(String name, double price, String category,String size,String material,int quantity) {
        super(name, price);
        this.category=category.toUpperCase();
        this.size=size.toUpperCase();
        this.material=material;
        this.quantity=quantity;
    }
    
    /**
     * Getters method
     * @return this.field
     */
    public int getQuantity(){
        return this.quantity;
    }

    public String getCategory(){
        return this.category;
    }
    
    public String getSize(){
        return this.size;
    }

    public String getMaterial(){
        return this.material;
    }
    public void setQuantity(int newQuantity){
        this.quantity = newQuantity;
    }
    
    @Override
    public String toString(){
        String s = ("Name: " + this.getName() + " | Price: " + this.getPrice() + " | Size: "+ this.getSize()+" | Material: " + this.getMaterial());
        return s;
    }

    /**
     * Creates a new ClothingItems object by taking user input.
     *
     * @return The newly created ClothingItems object.
     */
    public static ClothingItems addItem(){
        String category= "Default";
        String name = Helper.getValidStringInput("Enter product name (no special characters): ", "^[a-zA-Z0-9 ]*$");
        double price = Helper.getValidDoubleInputPrice("Enter product price: $");
        String size = Helper.getValidStringInput("Enter product size (M, L, S): ", "^[MLS]$");
        String material = Helper.getValidStringInput("Enter product material: ", "^[a-zA-Z ]*$");
        int qtyInStock = Helper.getValidIntInput("Enter product quantity in stock: ");
        ClothingItems item = new ClothingItems(name, price, category, size, material, qtyInStock);
        return item;
    }
    
    /**
     * Changes the item into a String/ For the purpose of separating the data and business layer
     * @return
     */
    public String ExportToString(){
        return this.getCategory()+","+this.getName()+","+this.getPrice()+","+this.getSize()+","+this.getMaterial()+","+this.getQuantity();
    }
}
