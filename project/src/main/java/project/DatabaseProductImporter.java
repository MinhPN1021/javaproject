package project;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;





public class DatabaseProductImporter implements IProductImporter {
    public List<ClothingItems> loadClothingItems(){
        List<ClothingItems> clothingItems = new ArrayList<>();
        try{
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca", "UserName","Password");
            String retrieveClothingStatement ="SELECT * FROM CLOTHINGITEMS";
            Statement  stm= conn.prepareStatement(retrieveClothingStatement);
            ResultSet rs =stm.executeQuery(retrieveClothingStatement);
            while(rs.next()){
                String category = rs.getString("Category");
                switch (category) {
                    case "COLD":
                        clothingItems.add(new ClothingColdItems(rs.getString("Name"), rs.getDouble("Price"), category, rs.getString("Size"), rs.getString("Material"), rs.getInt("Quantity"), rs.getString("Water Proofness"), rs.getDouble("Insulation"), rs.getInt("Layers"), rs.getString("Accessory")));
                        break;
                    case "HOT":
                        clothingItems.add(new ClothingHotItems(rs.getString("Name"), rs.getDouble("Price"), category, rs.getString("Size"), rs.getString("Material"), rs.getInt("Quantity"), rs.getString("HeatConductivity"), rs.getString("WaterAbsorbtion"), rs.getString("UVProtection")));
                        break;
                    default:
                        clothingItems.add(new ClothingItems(rs.getString("Name"), rs.getDouble("Price"), category, rs.getString("Size"), rs.getString("Material"), rs.getInt("Quantity")));
                        break;
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return clothingItems;
    }
    public static Connection getConnection(String url,String username, String password){
        throw new UnsupportedOperationException("TO DO");

    }
}
