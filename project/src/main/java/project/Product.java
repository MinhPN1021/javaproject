/**
 * @author Minh Phu Ngo
 */

package project;
/**
 * Abstract class Product, which is the first base type. 
 */
public abstract class Product {
    private String name;
    private double price;

    /**
     * Constructor for the product
     * @param name
     * @param price
     */
    public Product(String name, double price) {
        this.name = name;
        this.price=price;
    }
        
        
    /**
    * The getName() function returns the name of an object.
    * @returns name
    */
    public String getName(){
        return this.name;
    }

    /**
     * The getPrice() function returns the price of an object.
     * @returns price
     */
    public double getPrice(){
        return this.price;
    }
    
    

}
