/**
 * @author Minh Phu Ngo
 */

package project;
import static org.junit.Assert.*;


import java.util.ArrayList;
import java.util.List;


import org.junit.Test;
/**
 * ClothingItems test contains unit tests which checks if an item has been added
 */
public class ClothingItemsTest{
    @Test
    public void ConstructorTest(){
        double errMargin =0.000000000001;
        ClothingItems c1 = new ClothingItems("Shoes",20.99,"shoes","L","leather",20);
        assertEquals(c1.getName(),"Shoes");
        assertEquals(c1.getPrice(),20.99,errMargin);
        assertEquals(c1.getCategory(), "SHOES");
        assertEquals(c1.getSize(), "L");
        assertEquals(c1.getMaterial(), "leather");
        assertEquals(c1.getQuantity(), 20);
    }
    /**
     * Test adding a hot item to the list
     */
    @Test
    public void addHotItemsTest() {

        List<ClothingItems> clothingProducts = new ArrayList<ClothingItems>();  
       
        ClothingHotItems item = new ClothingHotItems("Lemon", 14.99, "hot", "S", "Cotton", 50, "Restricted", "Low", "High");
       
        
       double errMargin = 0.0000001;

        // Act
        clothingProducts.add(item);

        // Assert
        assertNotNull(item);
        assertEquals("Lemon", item.getName());
        assertEquals(14.99, item.getPrice(), errMargin);
        assertEquals("S", item.getSize());
        assertEquals("Cotton", item.getMaterial());
        assertEquals(50, item.getQuantity());
        assertEquals("Restricted", item.getHeatConductivity());
        assertEquals("Low", item.getWaterAbsorbtion());
        assertEquals("High", item.getUVProtection());

        
        assertTrue(clothingProducts.contains(item));
        
    }

    /**
     * unit test for adding a cold item
     */
    @Test
    public void addColdItemsTest() {

        List<ClothingItems> clothingProducts = new ArrayList<>();
     
        
        double errMargin = 0.0000001;

        // Act
        ClothingColdItems item = new ClothingColdItems("WinterCoat", 99.99, "cold", "L", "Wool", 20, "Waterproof", 0.5, 3, "Yes");
        clothingProducts.add(item);

        // Assert
        assertNotNull(item);
        assertEquals("WinterCoat", item.getName());
        assertEquals(99.99, item.getPrice(), errMargin);
        assertEquals("L", item.getSize());
        assertEquals("Wool", item.getMaterial());
        assertEquals(20, item.getQuantity());
        assertEquals("Waterproof", item.getWaterProofness());
        assertEquals(0.5, item.getInsulation(), errMargin);
        assertEquals(3, item.getLayers());
        assertEquals("Yes", item.getHaveAccessory());

        assertTrue(clothingProducts.contains(item));
        
    }

    /**
     * test for adding a regular item (not hot or cold)
     */
    @Test
    public void addRegularItemsTest() {

        List<ClothingItems> clothingProducts = new ArrayList<>();

        double errMargin = 0.0000001;

        ClothingItems item = new ClothingItems("CasualShirt", 29.99, "casual", "M", "Cotton", 15);
        clothingProducts.add(item);

        assertNotNull(item);
        assertEquals("CasualShirt", item.getName());
        assertEquals(29.99, item.getPrice(), errMargin);
        assertEquals("CASUAL", item.getCategory());
        assertEquals("M", item.getSize());
        assertEquals("Cotton", item.getMaterial());
        assertEquals(15, item.getQuantity());

        assertTrue(clothingProducts.contains(item));

    }



}