package project;
import java.util.Comparator;
import java.util.Collections;
import java.util.List;
import project.ISorter.SortAccessory;
import project.ISorter.SortByCategory;
import project.ISorter.SortMaterial;
import project.ISorter.SortSize;
import project.ISorter.SortUVProtection;
import project.ISorter.SortWaterAbsorbtion;

/**
 * A class that implements IDisplayer, displays different ways.
 */
public class EmployeeDisplayer implements IDisplayer {
    private List<ClothingItems> clothingItems;
    

    public EmployeeDisplayer(List<ClothingItems> clothingItems) {
        this.clothingItems = clothingItems;
    }

    /**
     * Displays clothing products for employees based on user choice.
     *
     * @param clothingItems The list of ClothingItems to be displayed.
     */
    @Override
    public void displayProducts() {
        
        int choice = displayActions();
        
        Comparator<ClothingItems> comparator = null;
        switch (choice) {
            case 1:
                comparator = new SortByCategory();
                break;
            case 2:
                comparator = new SortMaterial();
                break;
            case 3: 
                comparator = new SortSize();
                break;
            case 4: 
                comparator = new SortWaterAbsorbtion();
                break;
            case 5: 
                comparator= new SortUVProtection();
                break;
            case 6: 
                comparator = new SortAccessory();
                break;
            default:
                System.out.println("Invalid choice. Defaulting to sorting by Category.");
                comparator = new SortByCategory();
        }
        if (comparator != null) {
            Collections.sort(this.clothingItems, comparator);
            displaySortedProducts();
        }
    }
    public int displayActions(){
        System.out.println("Choose sorting option:");
        System.out.println("1. Sort by Category");
        System.out.println("2. Sort by Material");
        System.out.println("3. Sort by Size: L,M,S");
        System.out.println("4. Sort by water absorption");
        System.out.println("5. Sort by UVProtection: High/Low/Medium");
        System.out.println("6. Sort by having Accessory: YES/NO");
        return Helper.getValidInput(1, 6, "Enter the number corresponding to your choice:");
        
    }
    
    private void displaySortedProducts() {
        // Now you can display the sorted products or perform any other actions as needed
        for (ClothingItems item : this.clothingItems) {
            System.out.println(item.toString());
            System.out.println("Quantity: " + item.getQuantity());
            System.out.println();
        }
    }
}
