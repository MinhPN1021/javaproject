package project;
import java.util.*;

/**
 * ClothingColdItems class represents cold category clothing items and extends
 * the ClothingItems class. It includes additional properties specific to cold
 * items such as water proofness, insulation, layers, and accessory presence.
 */

public class ClothingColdItems extends ClothingItems{

    private String waterProofness;
    private double insulation;
    private int layers;
    private String haveAccessory;

    /**
     * Constructs a new ClothingColdItems object with the specified properties.
     *
     * @param name           The name of the clothing item.
     * @param price          The price of the clothing item.
     * @param category       The category of the clothing item.
     * @param size           The size of the clothing item.
     * @param material       The material of the clothing item.
     * @param quantity       The quantity of the clothing item in stock.
     * @param waterProofness The water proofness level of the cold clothing item.
     * @param insulation     The insulation level of the cold clothing item.
     * @param layers         The number of layers in the cold clothing item.
     * @param haveAccessory  Indicates whether the cold clothing item has an accessory.
     */
    public ClothingColdItems(String name, double price, String category,String size,String material,int quantity,String waterProofness,double insulation,int layers,String haveAccessory) {
        super(name, price, category, size, material, quantity);
        this.waterProofness = waterProofness;
        this.insulation = insulation;
        this.layers = layers;
        this.haveAccessory = haveAccessory;
    }
    /**
     * Getters method 
     * @return this.field 
     */
    public String getWaterProofness(){
        return this.waterProofness;
    }
    public double getInsulation(){
        return this.insulation;
    }
    public int getLayers(){
        return this.layers;
    }
    public String getHaveAccessory(){
        return this.haveAccessory;
    }
    @Override
    public String toString(){
        String s=("Name: " + this.getName() + " | Price: " + this.getPrice() + " | Size: "+ this.getSize()+" | Material: " + this.getMaterial() + " | Water Proof: " + this.getWaterProofness() + " | Insulation: " + this.getInsulation() + " | Layers: " + this.getLayers()) + " | Accessory: " + this.getHaveAccessory();
        return s;
    }
    /**
     * Creates a new ClothingColdItems object by taking user input.
     *
     * @return The newly created ClothingColdItems object.
     */
    public static ClothingColdItems addItem(){
        Scanner reader =new Scanner(System.in);

        String name = Helper.getValidStringInput("Enter product name (no special characters): ", "^[a-zA-Z0-9 ]*$");
        double price = Helper.getValidDoubleInputPrice("Enter product price: $");
        String size = Helper.getValidStringInput("Enter product size (M, L, S): ", "^[MLS]$");
        String material = Helper.getValidStringInput("Enter product material: ", "^[a-zA-Z ]*$");
        int qtyInStock = Helper.getValidIntInput("Enter product quantity in stock: ");
        String waterProofness = Helper.getValidStringInput("Enter product water proofness (IP1 - IP5): ", "^(IP[1-5])$");
        double insulation = Helper.getValidDoubleInput("Enter product insulation (degree 0-1): ", "^(?:0*(?:\\.\\d+)?|1(\\.0*)?)$");
        int numLayers = Helper.getValidIntInput("Enter number of layers: ");

        System.out.println("Does product have accessories?(Y/N)");
        String answer= reader.nextLine().toLowerCase();
        String hasAccessories= "";
        if (answer.equals("yes")|| answer.equals("y")){
            hasAccessories = "YES";
        }
        else{
             hasAccessories = "NO";
        }
        ClothingColdItems newItem = new ClothingColdItems(name, price, "COLD", size, material, qtyInStock, 
        waterProofness,insulation,numLayers,hasAccessories);
        
        return newItem;
    }

    /**
     * Changes the item into a String/ For the purpose of separating the data and business layer
     * @return
     */
    public String ExportToString(){
        return this.getCategory()+","+this.getName()+","+this.getPrice()+","+this.getSize()+","+this.getMaterial()+","+this.getQuantity()+","+this.getWaterProofness()+","+this.getInsulation()+","+this.getLayers()+","+this.getHaveAccessory();
    }

   
}
