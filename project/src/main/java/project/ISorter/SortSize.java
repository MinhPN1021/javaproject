package project.ISorter;
import java.util.*;

import project.ClothingItems;

/**
 * Class that sorts by Size using Comparator and compare.
 */
public class SortSize implements Comparator<ClothingItems>{
    @Override
    public int compare(ClothingItems item1, ClothingItems item2) {
        return item1.getSize().compareTo(item2.getSize());
    }
}
