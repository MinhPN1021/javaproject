package project;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A class for importing clothing products from a file.
 */
public class FileProductImporter implements IProductImporter {
    private Path filePath;
    private int counter = 0;
    /**
     * Constructor for FileProductImporter, contains a filePath
     * @param filePath
     */
    public FileProductImporter(Path filePath) {
        this.filePath = filePath;
    }

    /**
     * Loads clothing products from the specified file.
     *
     * @return A list of ClothingItems loaded from the file.
     */
    @Override
    public List<ClothingItems> loadClothingItems() {

        List<ClothingItems> clothingItems = new ArrayList<>();
        List<String> lines;
        try {
            lines = Files.readAllLines(filePath);
            if (lines.isEmpty()) {
                return clothingItems;
            }
            boolean firstLine = true;
            for (String line : lines) {

                if (firstLine) {
                    firstLine = false;
                    continue;
                }
                String[] parts = line.split(",");
                String category = parts[0].trim();
                String name = parts[1].trim();
                double price = parseDouble(parts[2].trim());
                String size = parts[3].trim();
                String material = parts[4].trim();
                int quantity = parseInt(parts[5].trim());

                switch (category) {
                    case "HOT":
                        addHotItem(clothingItems, name, price, size, material, quantity, parts[6].trim(), parts[7].trim(), parts[8].trim());
                        break;

                    case "COLD":
                        addColdItem(clothingItems, name, price, size, material, quantity, parts[6].trim(), parseDouble(parts[7].trim()), parseInt(parts[8].trim()), parts[9].trim());
                        break;

                    default:
                        ClothingItems item = new ClothingItems(name, price, category, size, material, quantity);
                        clothingItems.add(item);
                        break;
                }
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return clothingItems;
    }

    /**
     * Gets the counter value.
     *
     * @return The counter value.
     */
    public int getCounter() {
        return this.counter;
    }

    /**
     * Parses an integer from the given string.
     *
     * @param intStr The string containing the integer value.
     * @return The parsed integer.
     */
    private int parseInt(String intStr) {
        String numericPart = intStr.replaceAll("[^0-9.]", "");
        
        try {
            return Integer.parseInt(numericPart);
        } 
        catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Parses a double from the given string.
     *
     * @param doubleStr The string containing the double value.
     * @return The parsed double.
     */
    private double parseDouble(String doubleStr) {
        String numericPart = doubleStr.replaceAll("[^0-9.]", "");
        try {
            return Double.parseDouble(numericPart);
        } 
        catch (NumberFormatException e) {
            return 0.0;
        }
    }

    /**
     * Adds a hot item to the list
     * @param clothingItems
     * @param name
     * @param price
     * @param size
     * @param material
     * @param quantity
     * @param heatConductivity
     * @param waterAbsorption
     * @param uvProtection
     */
    private void addHotItem(List<ClothingItems> clothingItems, String name, double price, String size, String material, int quantity, String heatConductivity, String waterAbsorption, String uvProtection) {
        ClothingHotItems hotItem = new ClothingHotItems(name, price, "HOT", size, material, quantity, heatConductivity, waterAbsorption, uvProtection);
        clothingItems.add(hotItem);
    }
    
    /**
     * Adds a cold item to the list 
     * @param clothingItems
     * @param name
     * @param price
     * @param size
     * @param material
     * @param quantity
     * @param waterProofness
     * @param insulation
     * @param layers
     * @param haveAccessory
     */
    private void addColdItem(List<ClothingItems> clothingItems, String name, double price, String size, String material, int quantity, String waterProofness, double insulation, int layers, String haveAccessory) {
        ClothingColdItems coldItem = new ClothingColdItems(name, price, "COLD", size, material, quantity, waterProofness, insulation, layers, haveAccessory);
        clothingItems.add(coldItem);
    }
}
