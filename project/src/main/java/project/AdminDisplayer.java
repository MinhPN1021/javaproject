package project;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import project.ISorter.SortPriceDesc;

/**
 * AdminDisplayer class implements the IDisplayer interface and is responsible
 * for displaying clothing items in a sorted order based on price in descending
 * order.
 */
public class AdminDisplayer implements IDisplayer{
    private List<ClothingItems> clothingItems;

    public AdminDisplayer(List<ClothingItems> clothingItems){
        this.clothingItems=clothingItems;
    }
    /**
     * Displays the clothing items in a sorted order based on price in descending
     * order. The display includes the name, price, and quantity in stock for each
     * item.
     *
     * @param clothingItems List of ClothingItems to be displayed.
     */
    @Override
    public void displayProducts() {
        /* Already sorted, it immediately displays it as sorted by price. */

        SortPriceDesc priceComp = new SortPriceDesc();
        Collections.sort(this.clothingItems,priceComp);

        int index = 1;
        System.out.println("---List of products---");
        for (Product clothingItem : this.clothingItems) {
            System.out.println(index +". "+ clothingItem.toString() + " | Quantity in stock: " + ((ClothingItems) clothingItem).getQuantity());
            index++;
        }
        System.out.println("----------------------");
        int action =displayActions();
        switch (action) {
            case 1:
                
                addItem();
                displayProducts();
                break;
            case 2:
                
                break;
            default:
                break;
        }
        
    }
    Scanner reader = new Scanner(System.in);
    /**
     * Displays the available actions for the admin and prompts for selection.
     *
     * @return The selected action as an integer.
     */
    public int displayActions(){
        int choice = Helper.getValidInput(1, 2, "Please select an option\n1. Add Item\t2. Quit\t");
        
        return choice;
    }

    /**
     * Adds a new clothing item to the list based on the category provided by the admin.
     *
     * @param clothingItems The list of ClothingItems to which the new item will be added.
     */
    public void addItem(){
        String path = "project/src/main/java/project/data/testing1.txt";
        ClothingItems item = null;
        System.out.println("Enter a category: ");
        String category = reader.nextLine();
        switch (category.toUpperCase()) {
            case "HOT": 
                item = ClothingHotItems.addItem();    
            break;
            case "COLD":
                item = ClothingColdItems.addItem();
            break;
            default:
                item = ClothingItems.addItem();
            break;
        }
        this.clothingItems.add(item);
        Helper.writeToFile(path,item.ExportToString());
    }
}


