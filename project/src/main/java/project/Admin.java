package project;
import java.nio.file.*;
import java.io.IOException;
import java.io.File;
import java.util.*;

public class Admin 
{
    
    /** 
     * @param args
     */
    public static void main( String[] args ) throws IOException
    {

        Path filePath = Paths.get("project/src/main/java/project/data/testing1.txt");
        FileProductImporter importer = new FileProductImporter(filePath);
        

        List<ClothingItems> clothingItems = importer.loadClothingItems();

        /* Testing AdminDisplayer if it works */
        AdminDisplayer adminDisplayer = new AdminDisplayer(clothingItems);
        adminDisplayer.displayProducts();

        System.out.println(clothingItems.size());

        EmployeeDisplayer employeeDisplayer = new EmployeeDisplayer(clothingItems);
        employeeDisplayer.displayProducts();
    }

    

}
