package project;
import java.nio.file.*;
import java.io.IOException;
import java.util.*;

/**
 * Main class that Initiates all the logic of a Store
 */
public class Store 
{
    
    /** 
     * @param args
     */
    public static void main( String[] args ) throws IOException
    {

        Path filePath = Paths.get("project/src/main/java/project/data/testing1.txt");
        FileProductImporter importer = new FileProductImporter(filePath);
        List<ClothingItems> clothingItems = importer.loadClothingItems();
        boolean session = true;
        while (session){
            System.out.println("    /| |/|\\| |\\");
            System.out.println("   /_|  .|.  |_\\");
            System.out.println("     |  .|.  |");
            System.out.println("     |  .|.  |");
            System.out.println("     |__.|.__|");
            
            int choice = Helper.getValidInput(1, 4, "run store as:\n 1. Admin\n 2. Employee\n 3. Customer\n 4. Terminate the session");
            System.out.println("----------------------");
            switch (choice)
            {
                case 1:
                    AdminDisplayer adminDisplayer = new AdminDisplayer(clothingItems);
                    adminDisplayer.displayProducts();
                    break;
                case 2:
                    EmployeeDisplayer employeeDisplayer = new EmployeeDisplayer(clothingItems);
                    employeeDisplayer.displayProducts();
                    break;
                case 3:
                    CustomerDisplayer customerDisplayer = new CustomerDisplayer(clothingItems);
                    customerDisplayer.displayProducts();
                    session = false;
                    break;
                case 4:
                    System.out.println("Session is over");
                    session = false;
                    
                    
            }
           
            
        }

    }

    

}
