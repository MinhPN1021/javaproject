/**
 * @author Minh Phu Ngo
 */

package project;
import java.util.*;
import org.junit.Test;

import project.ISorter.SortAccessory;
import project.ISorter.SortByCategory;
import project.ISorter.SortMaterial;
import project.ISorter.SortName;
import project.ISorter.SortPriceAcs;
import project.ISorter.SortPriceDesc;
import project.ISorter.SortSize;
import project.ISorter.SortUVProtection;
import project.ISorter.SortWaterAbsorbtion;

import static org.junit.Assert.assertEquals;
/**
 * Class that contains unit tests mainly for Sorts 
 */
public class SortTest {
    double errMargin=0.0001;
    List<Product> products = new ArrayList<Product>();  
    List<ClothingItems> clothingProducts = new ArrayList<ClothingItems>();  
    List<ClothingHotItems> clothingHotProducts= new ArrayList<ClothingHotItems>();
    List<ClothingColdItems> clothingColdProducts = new ArrayList<ClothingColdItems>();
    ClothingItems c1 = new ClothingItems("Nike", 20,"hot","L","Leather",20);
    ClothingItems c2 = new ClothingItems("Adidas", 19,"cold","M","Vegan Leather",20);
    ClothingHotItems hot1= new ClothingHotItems("Lemon", 14.99, "hot", "S", "Cotton", 50, "Restricted", "Low", "High");
    ClothingHotItems hot2= new ClothingHotItems("Apple", 14.99, "hot", "M", "Cotton", 50, "Restricted", "High", "Low");
    ClothingHotItems hot3 = new ClothingHotItems("Apple", 14.99, "hot", "M", "Cotton", 50, "Restricted", "Medium", "Medium");
    ClothingColdItems coldItem1 = new ClothingColdItems("Adidas", 20, "cold", "M", "Leather", 20, "IP5", 0.25, 5, "NO");
    ClothingColdItems coldItem2 = new ClothingColdItems("Jacket", 19.99, "cold", "M", "Leather", 18, "IP4", 0.75, 2, "YES");

    /**
     * Unit test for sorting price descending
     */
    @Test
    public void SortPriceDescTest() {
        products.add(c1);
        products.add(c2);
        SortPriceDesc priceComp = new SortPriceDesc();
        Collections.sort(products,priceComp);
        assertEquals(20,products.get(0).getPrice(),errMargin);
        assertEquals(19,products.get(1).getPrice(),errMargin);
        


    }
    /**
     * Unit test for sorting price ascending
     */
    @Test
    public void SortPriceAcsTest() {
        products.add(c1);
        products.add(c2);
        SortPriceAcs priceComp = new SortPriceAcs();
        Collections.sort(products,priceComp);
        assertEquals(19,products.get(0).getPrice(),errMargin);
        assertEquals(20,products.get(1).getPrice(),errMargin);

    }

    /**
     * Unit test for Sorting Name alphabetically
     */
    @Test
    public void SortNameTest(){
        clothingProducts.add(c1);
        clothingProducts.add(c2);
        SortName nameComp = new SortName();
        Collections.sort(clothingProducts,nameComp);
        assertEquals("Adidas",clothingProducts.get(0).getName());
        assertEquals("Nike", clothingProducts.get(1).getName());
    }
    
    /**
     * Unit test for Sorting by category (by products cold - > hot)
     */
    @Test
    public void SortCategoryTest(){
        clothingProducts.add(c1);
        clothingProducts.add(c2);
        SortByCategory categoryComp = new SortByCategory();
        Collections.sort(clothingProducts,categoryComp);
        assertEquals("COLD",clothingProducts.get(0).getCategory());
        assertEquals("HOT", clothingProducts.get(1).getCategory());
    }

    /**
     * Unit test to sort by material alphabetically
     */
    @Test
    public void SortMaterialTest(){
        clothingProducts.add(c1);
        clothingProducts.add(c2);
        SortMaterial materialComp = new SortMaterial();
        Collections.sort(clothingProducts,materialComp);
        assertEquals("Leather",clothingProducts.get(0).getMaterial());
        assertEquals("Vegan Leather", clothingProducts.get(1).getMaterial());
    }
    /**
     * Unit test for sorting by water absorbtion (Low/Medium/High)
     */
    @Test
    public void SortWaterAbsorbTest(){
        clothingHotProducts.add(hot1);
        clothingHotProducts.add(hot2);
        clothingHotProducts.add(hot3);
        SortWaterAbsorbtion waterComp= new SortWaterAbsorbtion();
        Collections.sort(clothingHotProducts,waterComp);
        assertEquals("High", clothingHotProducts.get(0).getWaterAbsorbtion());
        assertEquals("Low", clothingHotProducts.get(1).getWaterAbsorbtion());
        assertEquals("Medium", clothingHotProducts.get(2).getWaterAbsorbtion());
    }

    /**
     * Unit test for sorting by UvProtection High/Low/Medium
     */
    @Test
    public void SortUVProtectionTest(){
        clothingHotProducts.add(hot1);
        clothingHotProducts.add(hot2);
        clothingHotProducts.add(hot3);
        SortUVProtection UVComp= new SortUVProtection();
        Collections.sort(clothingHotProducts,UVComp);
        assertEquals("High", clothingHotProducts.get(0).getUVProtection());
        assertEquals("Low", clothingHotProducts.get(1).getUVProtection());
        assertEquals("Medium", clothingHotProducts.get(2).getUVProtection());
    }

    /**
     * Unit test to sort by size L/M/S
     */
    @Test
    public void SortSizeTest(){
        clothingProducts.add(c1);
        clothingProducts.add(hot1);
        clothingProducts.add(hot2);
        SortSize sizeComp= new SortSize();
        Collections.sort(clothingProducts,sizeComp);
        assertEquals("L", clothingProducts.get(0).getSize());
        assertEquals("M", clothingProducts.get(1).getSize());
        assertEquals("S", clothingProducts.get(2).getSize());
    }

    /**
     * Unit test for sorting by accessory No -> YES
     */
    @Test
    public void SortAccessoryTest() {
        clothingColdProducts.add(coldItem1);
        clothingColdProducts.add(coldItem2);
        SortAccessory accessoryComp = new SortAccessory();
        Collections.sort(clothingProducts, accessoryComp);
        assertEquals("NO", clothingColdProducts.get(0).getHaveAccessory());
        assertEquals("YES", clothingColdProducts.get(1).getHaveAccessory());
    }
    
}
