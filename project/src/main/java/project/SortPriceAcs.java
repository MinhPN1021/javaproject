
package project;
import java.util.*;

public class SortPriceAcs  implements Comparator<Product> {
    @Override
    public int compare( Product item1, Product item2){
        return Double.compare(item1.getPrice(), item2.getPrice());
    }
    
}
