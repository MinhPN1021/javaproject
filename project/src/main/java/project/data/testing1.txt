Category - Name - Price - Size - Material - Quantity in stock - (Water Proofness-Insulation-Layers-Accessory) or (Heat Conductivity-Water Absorbtion-UV Protection)
COLD,Adidas,20.0,M,Leather,101,IP5,0.25,5,NO
COLD,Jacket,19.99,M,Leather,18,IP4,0.75,2,YES
HOT,Nikke,15.0,L,Wool,20,Breathable,None,High
HOT,Shorts,56.0,M,Linen,4,Breathable,Low,Medium
HOT,Balencitata,14.99,M,Coton,9,Restricted,None,Low
HOT,Pants,9.99,M,Coton,12,Breathable,None,High
HOT,Socks,5.99,M,Wool,15,Restricted,None,High
HOT,Hat,12.99,M,Polyester,2,Breathable,None,High
HOT,Shirt,29.99,M,Silk,6,Breathable,Low,Medium
DEFAULT,kmmm,20.0,L,wool,20
