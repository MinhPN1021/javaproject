package project;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.io.IOException;

/**
 * Class for Customer UI specifically
 */
public class CustomerDisplayer implements IDisplayer {
    private ArrayList<ClothingItems> Cart;
    private List<ClothingItems> clothingItems;

    public CustomerDisplayer(List<ClothingItems> clothingItems) {
        this.Cart = new ArrayList<ClothingItems>();
        this.clothingItems = clothingItems;
    }
    
    /**
     * Method to display the current products inside our store with their index
     */
    @Override
    public void displayProducts() {
    
        int index = 1;
        System.out.println("---List of products---");
        for (Product clothingItem : this.clothingItems) {
            System.out.println(index +". "+ clothingItem.toString());
            index++;
        }
        System.out.println("----------------------");
        int action =displayActions();
        switch (action) {
            case 1:
                addToCart();
                displayCart(Cart);
                break;
            case 2:
                // should show the menu again
                // Only terminate when the user chooses 4
                break;
            default:
                break;
        }
    }
    /**
     *Method to add to the Customer's cart
     */
    public void addToCart(){
        System.out.print("Select an index to add to cart: ");
        int choice = Helper.getValidInput(1, clothingItems.size(), "Enter a valid index!");
        System.out.print("Enter quantity: ");
        int quantity = Helper.getValidIntInput("Enter a valid integer!");
        System.out.println((this.clothingItems.get(choice-1)).getQuantity());
        if (this.clothingItems.get(choice-1).getQuantity()>= quantity){
            this.clothingItems.get(choice-1).setQuantity(this.clothingItems.get(choice-1).getQuantity()-quantity);
            for (int i = 0;i<quantity;i++){
                Cart.add(this.clothingItems.get(choice-1));
            }
            updateStocks("project/src/main/java/project/data/testing1.txt");
        }
        else{
            System.out.println("Not enough stock");
            addToCart();
        }
    }
    /**
     * Displays the available actions for the customer and prompts for selection.
     *
     * @return The selected action as an integer.
     */
    public int displayActions(){
        return Helper.getValidInput(1, 2, "Please select an option\n 1. Add to Cart\t2. Quit");
    }
    /**
     * Displays the Customer's cart with all the selected products
     * 
     * 
     */
    public void displayCart(List<ClothingItems>Cart){
        int index = 1;
        System.out.println("---------Cart---------");
        for (Product clothingItem : Cart) {
            System.out.println(index +". "+ clothingItem.toString());
            index++;
        }
        System.out.println("----------------------");
        calculatePriceCart();
        
    }

    /**
     * Method that calculates the total price for all the products in the Customer's cart
     * 
     * @return the price as a double
     */
    public double calculatePriceCart(){
        double sum=0;
        for (Product item : this.Cart) {
            sum=sum+ item.getPrice();
        }
        System.out.println("Your total price to pay is: " + sum);
        return Math.round(sum * 100.0) / 100.0;
    }

    /**
     * method to update the file, and the list
     * Modifies the stock of an item depending on the user's amount of items to buy
     */
    public void updateStocks(String p) {
        Path path = Paths.get(p);
        Helper.CleanFile(p);
            for (ClothingItems item: this.clothingItems){
                Helper.writeToFile(p,item.ExportToString());
            }
        
        
        
        
    }

}
