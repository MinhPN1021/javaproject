package project.ISorter;
import java.util.Comparator;

import project.ClothingColdItems;
import project.ClothingHotItems;
import project.ClothingItems;

/**
 * Class that sorts by WaterAbsorbtion using Comparator and compare.
 * It also sorts if the item is different instances
 */
public class SortWaterAbsorbtion implements Comparator<ClothingItems>{

    @Override
    public int compare(ClothingItems o1, ClothingItems o2) {
        if (o1 instanceof ClothingHotItems && o2 instanceof ClothingHotItems) {
            ClothingHotItems hotItem1 = (ClothingHotItems) o1;
            ClothingHotItems hotItem2 = (ClothingHotItems) o2;

            // Compare water absorption for hot items
            return hotItem1.getWaterAbsorbtion().compareTo(hotItem2.getWaterAbsorbtion());
        } else if (o1 instanceof ClothingColdItems && o2 instanceof ClothingColdItems) {
            // If both items are cold items, consider them equal
            return 0;
        } else if (o1 instanceof ClothingHotItems && o2 instanceof ClothingColdItems) {
            // Hot items come before cold items
            return -1;
        } else {
            // Cold items come after hot items
            return 1;
        }
    }
}
