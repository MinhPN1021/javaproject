package project.ISorter;

import java.util.Comparator;

import project.ClothingColdItems;
import project.ClothingHotItems;
import project.ClothingItems;

/**
 * Class that sorts by Accessory using Comparator and compare.
 * Sorts by hot and cold items too
 */
public class SortAccessory implements Comparator<ClothingItems> {
   
    @Override
    public int compare(ClothingItems item1, ClothingItems item2) {
        if (item1 instanceof ClothingColdItems && item2 instanceof ClothingColdItems) {
            ClothingColdItems coldItem1 = (ClothingColdItems) item1;
            ClothingColdItems coldItem2 = (ClothingColdItems) item2;
            // Compare by Accessory (Cold items first)
            return -coldItem1.getHaveAccessory().compareTo(coldItem2.getHaveAccessory());
        } else if (item1 instanceof ClothingHotItems && item2 instanceof ClothingHotItems) {
            // If both items are Hot items, maintain the original order
            return 0;
        } else if (item1 instanceof ClothingColdItems) {
            // Cold items come first
            return -1;
        } else if (item2 instanceof ClothingColdItems) {
            // Cold items come first
            return 1;
        }
        return 0;
    }

}